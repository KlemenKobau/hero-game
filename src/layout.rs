use crate::hexgrid::hex::Hex;

struct Orientation {
    f0: f64,
    f1: f64,
    f2: f64,
    f3: f64,
    b0: f64,
    b1: f64,
    b2: f64,
    b3: f64,
    start_angle: f64,
}

impl Orientation {
    fn new(
        f0: f64,
        f1: f64,
        f2: f64,
        f3: f64,
        b0: f64,
        b1: f64,
        b2: f64,
        b3: f64,
        start_angle: f64,
    ) -> Orientation {
        Orientation {
            f0,
            f1,
            f2,
            f3,
            b0,
            b1,
            b2,
            b3,
            start_angle,
        }
    }

    fn create_pointy() -> Orientation {
        Orientation::new(
            3.0_f64.sqrt(),
            3.0_f64.sqrt() / 2.0,
            0.0,
            3.0 / 2.0,
            3.0_f64.sqrt() / 3.0,
            -1.0 / 3.0,
            0.0,
            2.0 / 3.0,
            0.5,
        )
    }

    fn create_flat() -> Orientation {
        Orientation::new(
            3.0 / 2.0,
            0.0,
            3.0_f64.sqrt() / 2.0,
            3.0_f64.sqrt(),
            2.0 / 3.0,
            0.0,
            -1.0 / 3.0,
            3.0_f64.sqrt() / 3.0,
            0.0,
        )
    }
}

#[derive(Debug)]
pub struct Point {
    pub x: f64,
    pub y: f64,
    pub z: f64
}

pub struct Layout {
    orientation: Orientation,
    size: Point,
    origin: Point,
}

impl Layout {
    pub fn create_pointy() -> Layout {
        let size = Point {x: 1.0, y: 0.0, z: 1.0};
        let origin = Point {x: 0.0, y: 0.0, z: 0.0};
        let orientation = Orientation::create_pointy();

        Layout {orientation, size, origin}
    }
}

pub fn hex_to_pixel(layout: &Layout, hex: &Hex) -> Point {
    let orientation = &layout.orientation;
    let x = (orientation.f0 * hex.q() as f64 + orientation.f1 * hex.r() as f64) * layout.size.x as f64;
    let z = (orientation.f2 * hex.q() as f64 + orientation.f3 * hex.r() as f64) * layout.size.z as f64;

    Point {
        x: x + layout.origin.x,
        y: 0.0,
        z: z + layout.origin.z,
    }
}
