use std::collections::HashMap;
use bevy::pbr::wireframe::{Wireframe, WireframeConfig};
use bevy::prelude::{Assets, AssetServer, Bundle, Color, Commands, Component, ComputedVisibility, GlobalTransform, Handle, MaterialMeshBundle, Mesh, PbrBundle, Res, ResMut, StandardMaterial, Transform, Visibility};
use bevy::render::mesh::{Indices, PrimitiveTopology};
use bevy::scene::Scene;
use bevy_mod_picking::PickableBundle;
use crate::hexgrid::hex::Hex;
use crate::layout::{hex_to_pixel, Layout, Point};

#[derive(Component)]
struct HexComponent;

#[derive(Bundle)]
struct HexBundle {
    transform: Transform,
    mesh: Handle<Mesh>,
    visibility: Visibility,
    global_transform: GlobalTransform,
    computed_visibility: ComputedVisibility,
}

struct HexMapFactory {
    layout: Layout,
    hex_mesh_handle: Handle<Mesh>,
    hex_scene_handle: Handle<Scene>
}

impl HexMapFactory {
    fn new(mesh: Handle<Mesh>, scene: Handle<Scene>) -> HexMapFactory {
        let layout = Layout::create_pointy();
        HexMapFactory {layout, hex_mesh_handle: mesh, hex_scene_handle: scene}
    }

    fn create_hex_bundle(&self, hex: &Hex, height: &f64) -> HexBundle {
        let point = hex_to_pixel(&self.layout, hex);
        let mesh_handle = self.hex_mesh_handle.clone();
        let scene_handle = self.hex_scene_handle.clone();

        HexBundle {
            transform: Transform::from_xyz(point.x as f32, height.clone() as f32, point.z as f32),
            global_transform: GlobalTransform::from_xyz(point.x as f32, height.clone() as f32, point.z as f32),
            mesh: mesh_handle,
            visibility: Visibility::visible(),
            computed_visibility: ComputedVisibility::not_visible()
        }
    }
}

fn generate_height_map() -> HashMap<Hex, f64> {
    let mut map = HashMap::new();

    for i in 1..10 {
        for j in 1..10 {
            map.insert(Hex::new(i,j, -i -j), i as f64 / 10.0);
        }
    }

    map
}

fn create_hex_field_mesh() -> Mesh {
    let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);

    let sqrt_3 = 3.0_f64.sqrt();
    let top_point = Point {x: 0.0, y: 0.0, z: -0.5};
    let top_left = Point {x: -sqrt_3 / 2.0, y: 0.0, z: -0.25};
    let top_right = Point {x: sqrt_3 / 2.0, y: 0.0, z: -0.25};
    let bot_left = Point {x: -sqrt_3 / 2.0, y: 0.0, z: 0.25};
    let bot_right = Point {x: sqrt_3 / 2.0, y: 0.0, z: 0.25};
    let bot_point = Point {x: 0.0, y: 0.0, z: 0.5};

    let mut points = vec![top_point, top_right, bot_right, bot_point, bot_left, top_left];
    let mut lower_points: Vec<_> = points.iter().map(|p| Point {y: -1.0, ..*p }).collect();
    points.append(&mut lower_points);

    let point_threes: Vec<_> = points.iter().map(|p| [p.x as f32, p.y as f32, p.z as f32]).collect();

    let mut top_triangles = vec![[0, 4, 5],[0, 3, 4], [0, 1, 3], [1, 2, 3]];
    let mut bot_triangles: Vec<_> = top_triangles.iter().map(|x| x.map(|v| v + 6)).collect();

    let mut side_triangles = Vec::new();
    for i in 0..6 {
        let triangle_up = [i, (i + 1) % 6, (i + 1) % 6 + 6];
        let triangle_down = [i, i + 6, (i + 1) % 6 + 6];

        side_triangles.push(triangle_up);
        side_triangles.push(triangle_down);
    }

    let mut all_triangles: Vec<_> = Vec::new();
    all_triangles.append(&mut top_triangles);
    all_triangles.append(&mut bot_triangles);
    all_triangles.append(&mut side_triangles);

    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, point_threes);
    mesh.set_indices(Some(Indices::U32(all_triangles.concat())));
    mesh
}

pub fn create_default_map(mut commands: Commands, ass: Res<AssetServer>,  mut meshes: ResMut<Assets<Mesh>>) {

    let height_map = generate_height_map();
    let my_gltf = ass.load("Hex.glb#Scene0");
    let mesh_handle = meshes.add(Mesh::from(create_hex_field_mesh()));
    let hex_map_factory = HexMapFactory::new(mesh_handle, my_gltf);

    for (hex, height) in height_map.iter() {

        commands.spawn()
            .insert(HexComponent)
            .insert(Wireframe)
            .insert_bundle(hex_map_factory.create_hex_bundle(hex, height))
            .insert_bundle(PickableBundle::default());
    }
}