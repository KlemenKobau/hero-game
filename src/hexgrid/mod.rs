pub mod hex;
pub mod hex_directions;

use hex::Hex;
use hex_directions::HexDirections;

fn hex_length(hex: &Hex) -> i32 {
    (hex.q().abs() + hex.r().abs() + hex.s().abs()) / 2
}

fn hex_distance(a: &Hex, b: &Hex) -> i32 {
    hex_length(&(a - b))
}

fn hex_neighbour(hex: &Hex, hex_direction: &HexDirections) -> Hex {
    hex + &hex_direction.value()
}