use crate::hexgrid::Hex;

pub enum HexDirections {
    Top,
    TopLeft,
    TopRight,
    BotLeft,
    BotRight,
    Bot,
}

// flat layout
impl HexDirections {
    pub fn value(&self) -> Hex {
        match self {
            Top => Hex::new(0, -1, 1),
            TopLeft => Hex::new(-1, 0, 1),
            TopRight => Hex::new(1, -1, 0),
            BotLeft => Hex::new(-1, 1, 0),
            BotRight => Hex::new(1, 0, -1),
            Bot => Hex::new(0, 1, -1),
        }
    }
}