use std::ops::{Add, Mul, Sub};

#[derive(PartialEq, Eq, Copy, Clone, Hash)]
pub struct Hex {
    q: i32,
    r: i32,
    s: i32
}

impl Hex {
    pub fn new(q: i32, r: i32, s: i32) -> Self {
        if &q + &r + &s != 0 {
            panic!("Invalid coordinates for Hex");
        }

        Self { q, r, s }
    }

    pub fn q(&self) -> i32 {
        self.q
    }

    pub fn r(&self) -> i32 {
        self.r
    }
    pub fn s(&self) -> i32 {
        self.s
    }
}

impl Add for Hex {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self::new(self.q + other.q, self.r + other.r, self.s + other.s)
    }
}

impl Add for &Hex {
    type Output = Hex;

    fn add(self, other: Self) -> Hex {
        Hex::new(self.q + other.q, self.r + other.r, self.s + other.s)
    }
}

impl Sub for Hex {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self::new(self.q - other.q, self.r - other.r, self.s - other.s)
    }
}

impl Sub for &Hex {
    type Output = Hex;

    fn sub(self, other: Self) -> Hex {
        Hex::new(self.q - other.q, self.r - other.r, self.s - other.s)
    }
}

impl Mul<i32> for Hex {
    type Output = Self;

    fn mul(self, k: i32) -> Self {
        Self::new(self.q * k, self.r * k, self.s * k)
    }
}