use bevy::prelude::{App, Plugin};
use camera_movement::camera_movement;
use camera::spawn_camera;

mod camera;
mod camera_movement;

pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(camera_movement)
            .add_startup_system(spawn_camera);
    }
}