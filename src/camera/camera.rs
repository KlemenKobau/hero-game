use bevy::prelude::{Camera3dBundle, Component, Commands, default, Transform, Vec3};
use bevy_mod_picking::PickingCameraBundle;

#[derive(Component)]
pub struct MainCamera;

pub fn spawn_camera(mut commands: Commands) {

    // camera
    commands
        .spawn_bundle(Camera3dBundle {
            transform: Transform::from_xyz(0.0, 2.5, 1.0).looking_at(Vec3::ZERO, Vec3::Y),
            ..default()
        })
        .insert_bundle(PickingCameraBundle::default())
        .insert(MainCamera);
}