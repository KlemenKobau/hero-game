use crate::camera::camera::MainCamera;
use bevy::prelude::{Input, KeyCode, Query, Res, Time, Transform, Vec3, With};

pub fn camera_movement(
    input: Res<Input<KeyCode>>,
    time: Res<Time>,
    mut query: Query<&mut Transform, With<MainCamera>>,
) {
    let mut transform = query.single_mut();
    let rotation = transform.rotation;

    let mut direction = Vec3::ZERO;
    if input.pressed(KeyCode::Up) {
        direction.z -= 1.0;
    }
    if input.pressed(KeyCode::Down) {
        direction.z += 1.0;
    }
    if input.pressed(KeyCode::Left) {
        direction.x -= 1.0;
    }
    if input.pressed(KeyCode::Right) {
        direction.x += 1.0;
    }

    let mut movement =  rotation * direction;
    movement.y = 0.0;

    if input.pressed(KeyCode::Plus) {
        movement.y += 1.0;
    }
    if input.pressed(KeyCode::Minus) {
        movement.y -= 1.0;
    }

    movement = movement.normalize_or_zero();

    transform.translation += movement * 2.0 * time.delta_seconds();
}
