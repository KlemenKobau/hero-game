use bevy::pbr::wireframe::WireframePlugin;
use bevy::prelude::*;
use bevy_mod_picking::{DebugCursorPickingPlugin, DefaultPickingPlugins};
use crate::map::create_default_map;
use crate::camera::CameraPlugin;

mod hexgrid;
mod layout;
mod map;
mod camera;

fn spawn_environment(mut commands: Commands) {
    // light
    commands.spawn_bundle(PointLightBundle {
        point_light: PointLight {
            intensity: 1500.0,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform::from_xyz(4.0, 8.0, 4.0),
        ..default()
    });
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(WireframePlugin)
        .add_plugin(CameraPlugin)
        .add_plugins(DefaultPickingPlugins)
        .add_plugin(DebugCursorPickingPlugin)
        .add_startup_system(spawn_environment)
        .add_startup_system(create_default_map)
        .run();
}
